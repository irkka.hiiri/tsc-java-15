# Task Manager 

Training project.  
A simple console application for managing task lists.

## TECH STACK

Java Core

## SOFTWARE

* JDK 1.8
* Maven 3.6.3
* Windows 10 64-bit/Linux Ubuntu 18.04 x86_64

## HARDWARE

* Core i7-4790 or Ryzen 3 3200G.
* GTX 1060 6GB, GTX 1660 Super (or R9 Fury)
* 12GB RAM.
* 6GB VRAM.
* 70GB SSD storage.

## BUILD 

```
mvn clean install
```

## RUN   

```
java -jar ./target/task-manager.jar
```

## DEVELOPER 

Irina Chaplygina  
Technoserv Consulting  
ichaplygina@tsconsulting.com

## SCREENSHOTS

### CRUD   

![](doc/1.png)      
![](doc/2.png)    
![](doc/3.png)       
![](doc/4.png) 

### STATUS   

![](doc/5.png)        
![](doc/6.png)    

### RELATIONS   

![](doc/7.png)    
![](doc/8.png)     
![](doc/9.png)     
![](doc/10.png)     
![](doc/11.png)     
