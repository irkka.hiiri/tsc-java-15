package ru.tsc.ichaplygina.taskmanager;

import ru.tsc.ichaplygina.taskmanager.bootstrap.Bootstrap;

public class TaskManager {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
