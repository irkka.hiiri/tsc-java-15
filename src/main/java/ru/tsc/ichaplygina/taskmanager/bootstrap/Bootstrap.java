package ru.tsc.ichaplygina.taskmanager.bootstrap;

import ru.tsc.ichaplygina.taskmanager.api.controller.ICommandController;
import ru.tsc.ichaplygina.taskmanager.api.controller.IProjectController;
import ru.tsc.ichaplygina.taskmanager.api.controller.IProjectTaskController;
import ru.tsc.ichaplygina.taskmanager.api.controller.ITaskController;
import ru.tsc.ichaplygina.taskmanager.api.repository.ICommandRepository;
import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.ICommandService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.controller.CommandController;
import ru.tsc.ichaplygina.taskmanager.controller.ProjectController;
import ru.tsc.ichaplygina.taskmanager.controller.ProjectTaskController;
import ru.tsc.ichaplygina.taskmanager.controller.TaskController;
import ru.tsc.ichaplygina.taskmanager.exception.entity.CommandNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.repository.CommandRepository;
import ru.tsc.ichaplygina.taskmanager.repository.ProjectRepository;
import ru.tsc.ichaplygina.taskmanager.repository.TaskRepository;
import ru.tsc.ichaplygina.taskmanager.service.CommandService;
import ru.tsc.ichaplygina.taskmanager.service.ProjectService;
import ru.tsc.ichaplygina.taskmanager.service.ProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.service.TaskService;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

import static ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.COMMAND_PROMPT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private void initData() {
        taskRepository.add(new Task("aaa", "Sample Task"));
        taskRepository.add(new Task("ggg", "Sample Task"));
        taskRepository.add(new Task("ccc", "Sample Task"));
        taskRepository.add(new Task("ttt", "Sample Task"));
        taskRepository.add(new Task("bbb", "Sample Task"));
        projectRepository.add(new Project("eee", "Sample Project"));
        projectRepository.add(new Project("yyy", "Sample Project"));
        projectRepository.add(new Project("xxx", "Sample Project"));
        projectRepository.add(new Project("333", "Sample Project"));
        projectRepository.add(new Project("hhh", "Sample Project"));
    }

    public void run(final String... args) {
        if (args == null || args.length == 0) processInput();
        else
            try {
                executeCommand(args);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
    }

    public void processInput() {
        initData();
        commandController.showWelcome();
        String command = readCommand();
        while (true) {
            try {
                executeCommand(command);
            } catch (Exception e) {
                printLinesWithEmptyLine(e.getMessage());
            }
            command = readCommand();
        }
    }

    public void executeCommand(final String command) throws Exception {
        if (command.isEmpty()) return;
        switch (command) {
            case TASKS_LIST:
                taskController.showList();
                break;
            case TASK_CREATE:
                taskController.create();
                break;
            case TASKS_CLEAR:
                taskController.clear();
                break;
            case TASKS_LIST_BY_PROJECT:
                projectTaskController.showTaskListByProjectId();
                break;
            case PROJECTS_LIST:
                projectController.showList();
                break;
            case PROJECT_CREATE:
                projectController.create();
                break;
            case PROJECTS_CLEAR:
                projectTaskController.clearAllProjects();
                break;
            case PROJECT_SHOW_BY_ID:
                projectController.showById();
                break;
            case PROJECT_SHOW_BY_INDEX:
                projectController.showByIndex();
                break;
            case PROJECT_SHOW_BY_NAME:
                projectController.showByName();
                break;
            case PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case PROJECT_REMOVE_BY_INDEX:
                projectTaskController.removeProjectByIndex();
                break;
            case PROJECT_REMOVE_BY_ID:
                projectTaskController.removeProjectById();
                break;
            case PROJECT_REMOVE_BY_NAME:
                projectTaskController.removeProjectByName();
                break;
            case PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case PROJECT_START_BY_NAME:
                projectController.startByName();
                break;
            case PROJECT_COMPLETE_BY_INDEX:
                projectController.completeByIndex();
                break;
            case PROJECT_COMPLETE_BY_ID:
                projectController.completeById();
                break;
            case PROJECT_COMPLETE_BY_NAME:
                projectController.completeByName();
                break;
            case TASK_ADD_TO_PROJECT:
                projectTaskController.addTaskToProject();
                break;
            case TASK_REMOVE_FROM_PROJECT:
                projectTaskController.removeTaskFromProject();
                break;
            case TASK_SHOW_BY_ID:
                taskController.showById();
                break;
            case TASK_SHOW_BY_INDEX:
                taskController.showByIndex();
                break;
            case TASK_SHOW_BY_NAME:
                taskController.showByName();
                break;
            case TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case TASK_REMOVE_BY_NAME:
                taskController.removeByName();
                break;
            case TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case TASK_START_BY_ID:
                taskController.startById();
                break;
            case TASK_START_BY_NAME:
                taskController.startByName();
                break;
            case TASK_COMPLETE_BY_INDEX:
                taskController.completeByIndex();
                break;
            case TASK_COMPLETE_BY_ID:
                taskController.completeById();
                break;
            case TASK_COMPLETE_BY_NAME:
                taskController.completeByName();
                break;
            case CMD_ABOUT:
                commandController.showAbout();
                break;
            case CMD_VERSION:
                commandController.showVersion();
                break;
            case CMD_HELP:
                commandController.showHelp();
                break;
            case CMD_INFO:
                commandController.showSystemInfo();
                break;
            case CMD_LIST_COMMANDS:
                commandController.showCommands();
                break;
            case CMD_LIST_ARGUMENTS:
                commandController.showArguments();
                break;
            case CMD_EXIT:
                commandController.exit();
                break;
            default:
                commandController.showUnknown(command);
        }
    }

    public void executeCommand(final String[] params) throws CommandNotFoundException {
        if (params == null || params.length == 0) return;
        switch (params[0]) {
            case ARG_ABOUT:
                commandController.showAbout();
                break;
            case ARG_VERSION:
                commandController.showVersion();
                break;
            case ARG_HELP:
                commandController.showHelp();
                break;
            case ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showUnknown(params[0]);
        }
    }

    public String readCommand() {
        return TerminalUtil.readLine(COMMAND_PROMPT);
    }

}
