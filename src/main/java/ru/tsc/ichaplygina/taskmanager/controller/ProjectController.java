package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.controller.IProjectController;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Comparator;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    private void showUpdateResult() {
        printLinesWithEmptyLine(PROJECT_UPDATED);
    }

    @Override
    public void create() throws NameEmptyException {
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        projectService.add(name, description);
        printLinesWithEmptyLine(PROJECT_CREATED);
    }

    @Override
    public void showList() {
        if (projectService.isEmpty()) {
            printLinesWithEmptyLine(NO_PROJECTS_FOUND);
            return;
        }
        final Comparator<Project> comparator = readComparator();
        printListWithIndexes(projectService.findAll(comparator));
    }

    @Override
    public void showProject(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        printLinesWithEmptyLine(project);
    }

    @Override
    public void showById() throws ProjectNotFoundException, IdEmptyException {
        final String id = readLine(ID_INPUT);
        final Project project = projectService.findById(id);
        showProject(project);
    }

    @Override
    public void showByIndex() throws ProjectNotFoundException, IndexIncorrectException {
        final int index = readNumber(INDEX_INPUT);
        final Project project = projectService.findByIndex(index - 1);
        showProject(project);
    }

    @Override
    public void showByName() throws ProjectNotFoundException, NameEmptyException {
        final String name = readLine(NAME_INPUT);
        final Project project = projectService.findByName(name);
        showProject(project);
    }

    @Override
    public void completeByName() throws IdEmptyException, ProjectNotFoundException, NameEmptyException {
        final String name = readLine(NAME_INPUT);
        projectService.completeByName(name);
        showUpdateResult();
    }

    @Override
    public void completeById() throws IdEmptyException, ProjectNotFoundException {
        final String id = readLine(ID_INPUT);
        projectService.completeById(id);
        showUpdateResult();
    }

    @Override
    public void completeByIndex() throws IdEmptyException, ProjectNotFoundException, IndexIncorrectException {
        final int index = readNumber(INDEX_INPUT);
        projectService.completeByIndex(index - 1);
        showUpdateResult();
    }

    @Override
    public void startByName() throws IdEmptyException, ProjectNotFoundException, NameEmptyException {
        final String name = readLine(NAME_INPUT);
        projectService.startByName(name);
        showUpdateResult();
    }

    @Override
    public void startById() throws IdEmptyException, ProjectNotFoundException {
        final String id = readLine(ID_INPUT);
        projectService.startById(id);
        showUpdateResult();
    }

    @Override
    public void startByIndex() throws IdEmptyException, ProjectNotFoundException, IndexIncorrectException {
        final int index = readNumber(INDEX_INPUT);
        projectService.startByIndex(index - 1);
        showUpdateResult();
    }

    @Override
    public void updateById() throws ProjectNotFoundException, IdEmptyException, NameEmptyException {
        final String id = readLine(ID_INPUT);
        if (projectService.findById(id) == null) throw new ProjectNotFoundException();
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        projectService.updateById(id, name, description);
        showUpdateResult();
    }

    @Override
    public void updateByIndex() throws ProjectNotFoundException, IndexIncorrectException, IdEmptyException, NameEmptyException {
        final int index = readNumber(INDEX_INPUT);
        if (projectService.findByIndex(index - 1) == null) throw new ProjectNotFoundException();
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        projectService.updateByIndex(index - 1, name, description);
        showUpdateResult();
    }

}
