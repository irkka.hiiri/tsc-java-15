package ru.tsc.ichaplygina.taskmanager.exception.empty;

public class IdEmptyException extends Exception {

    public IdEmptyException() {
        super("Error! Id is empty.");
    }

}
