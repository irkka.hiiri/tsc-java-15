package ru.tsc.ichaplygina.taskmanager.exception.empty;

public class NameEmptyException extends Exception {

    public NameEmptyException() {
        super("Error! Name is empty.");
    }

}
