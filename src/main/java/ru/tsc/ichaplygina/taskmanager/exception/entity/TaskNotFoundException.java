package ru.tsc.ichaplygina.taskmanager.exception.entity;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.TASK_NOT_FOUND;

public class TaskNotFoundException extends Exception {

    public TaskNotFoundException() {
        super(TASK_NOT_FOUND);
    }

}
