package ru.tsc.ichaplygina.taskmanager.exception.incorrect;

public class IndexIncorrectException extends Exception {

    public IndexIncorrectException() {
        super("Error! Incorrect index.");
    }

    public IndexIncorrectException(final int index) {
        super("Error! Incorrect index " + index);
    }

}
