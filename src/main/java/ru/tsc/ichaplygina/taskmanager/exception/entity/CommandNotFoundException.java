package ru.tsc.ichaplygina.taskmanager.exception.entity;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.APP_UNKNOWN_CMD;

public class CommandNotFoundException extends Exception {

    public CommandNotFoundException() {
        super(APP_UNKNOWN_CMD);
    }

    public CommandNotFoundException(final String command) {
        super(APP_UNKNOWN_CMD + command);
    }

}
