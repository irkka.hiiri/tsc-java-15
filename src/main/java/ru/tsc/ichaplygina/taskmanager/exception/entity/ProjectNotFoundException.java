package ru.tsc.ichaplygina.taskmanager.exception.entity;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.PROJECT_NOT_FOUND;

public class ProjectNotFoundException extends Exception {

    public ProjectNotFoundException() {
        super(PROJECT_NOT_FOUND);
    }

}
