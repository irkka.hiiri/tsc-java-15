package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Task add(final String name, final String description) throws NameEmptyException;

    void clear();

    Task completeById(final String id) throws IdEmptyException, TaskNotFoundException;

    Task completeByIndex(final int index) throws IndexIncorrectException, IdEmptyException, TaskNotFoundException;

    Task completeByName(final String name) throws NameEmptyException, IdEmptyException, TaskNotFoundException;

    List<Task> findAll(final Comparator<Task> comparator);

    Task findById(final String id) throws IdEmptyException;

    Task findByName(final String name) throws NameEmptyException;

    Task findByIndex(final int index) throws IndexIncorrectException;

    int getSize();

    boolean isEmpty();

    Task removeById(final String id) throws IdEmptyException;

    Task removeByIndex(final int index) throws IndexIncorrectException;

    Task removeByName(final String name) throws NameEmptyException;

    Task startById(final String id) throws IdEmptyException, TaskNotFoundException;

    Task startByIndex(final int index) throws IndexIncorrectException, IdEmptyException, TaskNotFoundException;

    Task startByName(final String name) throws NameEmptyException, IdEmptyException, TaskNotFoundException;

    Task updateByIndex(final int index, final String name, final String description)
            throws IndexIncorrectException, TaskNotFoundException, IdEmptyException, NameEmptyException;

    Task updateById(final String id, final String name, final String description)
            throws IdEmptyException, NameEmptyException, TaskNotFoundException;

    Task updateStatus(String id, Status status) throws IdEmptyException, TaskNotFoundException;

}
