package ru.tsc.ichaplygina.taskmanager.api.controller;

import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;

public interface IProjectTaskController {

    void addTaskToProject() throws IdEmptyException, ProjectNotFoundException, TaskNotFoundException;

    void clearAllProjects();

    void removeProjectById() throws IdEmptyException, ProjectNotFoundException;

    void removeProjectByIndex() throws IndexIncorrectException, IdEmptyException, ProjectNotFoundException;

    void removeProjectByName() throws IdEmptyException, NameEmptyException, ProjectNotFoundException;

    void removeTaskFromProject() throws IdEmptyException, ProjectNotFoundException, TaskNotFoundException;

    void showTaskListByProjectId() throws IdEmptyException, ProjectNotFoundException;

}
