package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll(Comparator<Project> comparator);

    Project add(final String name, final String description) throws NameEmptyException;

    Project completeById(final String id) throws IdEmptyException, ProjectNotFoundException;

    Project completeByIndex(final int index) throws IndexIncorrectException, IdEmptyException, ProjectNotFoundException;

    Project completeByName(final String name) throws NameEmptyException, IdEmptyException, ProjectNotFoundException;

    Project findById(final String id) throws IdEmptyException;

    Project findByName(final String name) throws NameEmptyException;

    Project findByIndex(final int index) throws IndexIncorrectException;

    boolean isEmpty();

    Project startById(final String id) throws IdEmptyException, ProjectNotFoundException;

    Project startByIndex(final int index) throws IdEmptyException, ProjectNotFoundException, IndexIncorrectException;

    Project startByName(final String name) throws NameEmptyException, IdEmptyException, ProjectNotFoundException;

    Project updateByIndex(final int index, final String name, final String description)
            throws IndexIncorrectException, IdEmptyException, NameEmptyException, ProjectNotFoundException;

    Project updateById(final String id, final String name, final String description)
            throws IdEmptyException, NameEmptyException, ProjectNotFoundException;

    Project updateStatus(String id, Status status) throws IdEmptyException, ProjectNotFoundException;

}
