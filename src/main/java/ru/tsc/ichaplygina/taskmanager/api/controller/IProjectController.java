package ru.tsc.ichaplygina.taskmanager.api.controller;

import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

public interface IProjectController {

    void completeById() throws IdEmptyException, ProjectNotFoundException;

    void completeByIndex() throws IdEmptyException, ProjectNotFoundException, IndexIncorrectException;

    void completeByName() throws IdEmptyException, ProjectNotFoundException, NameEmptyException;

    void create() throws NameEmptyException;

    void showProject(Project project) throws ProjectNotFoundException;

    void showById() throws ProjectNotFoundException, IdEmptyException;

    void showByIndex() throws ProjectNotFoundException, IndexIncorrectException;

    void showByName() throws ProjectNotFoundException, NameEmptyException;

    void showList();

    void startById() throws IdEmptyException, ProjectNotFoundException;

    void startByIndex() throws IdEmptyException, ProjectNotFoundException, IndexIncorrectException;

    void startByName() throws IdEmptyException, ProjectNotFoundException, NameEmptyException;

    void updateById() throws ProjectNotFoundException, IdEmptyException, NameEmptyException;

    void updateByIndex() throws ProjectNotFoundException, IndexIncorrectException, IdEmptyException, NameEmptyException;

}
