package ru.tsc.ichaplygina.taskmanager.api.repository;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    boolean isFoundById(String id);

    void add(final Project project);

    Project update(String id, String name, String description);

    void clear();

    Project findById(final String id);

    Project findByName(final String name);

    Project findByIndex(final int index);

    String getId(final String name);

    String getId(final int index);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    int getSize();

    boolean isEmpty();

    void remove(final Project project);

    Project updateStatus(String id, Status status);

    Project removeById(final String id);

    Project removeByIndex(final int index);

    Project removeByName(final String name);

}
