package ru.tsc.ichaplygina.taskmanager.api.controller;

import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

public interface ITaskController {

    void clear();

    void completeById() throws IdEmptyException, ProjectNotFoundException, TaskNotFoundException;

    void completeByIndex() throws IdEmptyException, ProjectNotFoundException, IndexIncorrectException, TaskNotFoundException;

    void completeByName() throws IdEmptyException, ProjectNotFoundException, NameEmptyException, TaskNotFoundException;

    void create() throws NameEmptyException;

    void removeById() throws IdEmptyException, TaskNotFoundException;

    void removeByIndex() throws IndexIncorrectException, TaskNotFoundException;

    void removeByName() throws NameEmptyException, TaskNotFoundException;

    void showTask(Task task) throws TaskNotFoundException;

    void showById() throws TaskNotFoundException, IdEmptyException;

    void showByIndex() throws TaskNotFoundException, IndexIncorrectException;

    void showByName() throws TaskNotFoundException, NameEmptyException;

    void showList();

    void startById() throws IdEmptyException, ProjectNotFoundException, TaskNotFoundException;

    void startByIndex() throws IdEmptyException, ProjectNotFoundException, IndexIncorrectException, TaskNotFoundException;

    void startByName() throws IdEmptyException, ProjectNotFoundException, NameEmptyException, TaskNotFoundException;

    void updateById() throws TaskNotFoundException, IdEmptyException, NameEmptyException;

    void updateByIndex() throws TaskNotFoundException, IndexIncorrectException, IdEmptyException, NameEmptyException;

}
