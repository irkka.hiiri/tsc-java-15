package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectTaskService {

    void clearProjects();

    int getProjectSize();

    Task removeTaskFromProject(String projectId, String taskId) throws TaskNotFoundException, IdEmptyException, ProjectNotFoundException;

    List<Task> findAllTasksByProjectId(String projectId, final Comparator<Task> taskComparator) throws IdEmptyException;

    Project removeProjectById(final String projectId) throws IdEmptyException;

    Project removeProjectByIndex(final int projectIndex) throws IndexIncorrectException, IdEmptyException;

    Project removeProjectByName(final String projectName) throws NameEmptyException, IdEmptyException, ProjectNotFoundException;

    Task addTaskToProject(String projectId, String taskId) throws IdEmptyException, TaskNotFoundException, ProjectNotFoundException;

}
