package ru.tsc.ichaplygina.taskmanager.model;

public class Command {

    private String name;

    private String arg;

    private String description;

    public Command() {
    }

    public Command(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Command(String name, String description, String arg) {
        this.name = name;
        this.description = description;
        this.arg = arg;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder().append(name != null && !name.isEmpty() ? name : "")
                .append(arg != null && !arg.isEmpty() ? " [" + arg + "]" : "")
                .append(description != null && !description.isEmpty() ? " - " + description + "." : "");
        return (stringBuilder.toString());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
